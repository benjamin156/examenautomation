import URL from "../../fixtures/URLs.json"

describe ('Compra en linea', () => {
    beforeEach(()=> {
        cy.visit(URL.URLs.Test)
       
    })

    it('Compra en linea', () => { 

        //1. Login       
        cy.url().should("eq", "https://www.saucedemo.com/")
        cy.get('[placeholder="Username"]').type('standard_user')
        cy.get('[placeholder="Password"]').type('secret_sauce') 
        cy.get('#login-button').click()

        //2. Agregar al carrito los productos Sauce Labs Backpack y Sauce Labs Fleece Jacket
        cy.get('#add-to-cart-sauce-labs-backpack').click()
        cy.get('#add-to-cart-sauce-labs-fleece-jacket').click()

        //3. Ir al carrito.
        cy.get('div.page_wrapper div:nth-child(1) div.header_container:nth-child(1) div.primary_header div.shopping_cart_container:nth-child(3) > a.shopping_cart_link').click()

        //4. Hacer aserciones en el carrito validando el nombre de los productos.
        //cy.get('[class="cart_item"]').should('have.length', 2) //validar que solo hay dos elementos en el carrito
        cy.get('div.page_wrapper div.cart_contents_container div.cart_list div.cart_item:nth-child(3) div.cart_item_label a:nth-child(1) > div.inventory_item_name').should('have.text', 'Sauce Labs Backpack')
        cy.get('div.page_wrapper div.cart_contents_container div.cart_list div.cart_item:nth-child(4) div.cart_item_label a:nth-child(1) > div.inventory_item_name').should('have.text', 'Sauce Labs Fleece Jacket')

        //5. Remover el último producto del carrito.
        cy.get('[class="cart_item_label"]').last().contains('Remove').click()

        //6. Click en el botón checkout.
        cy.get('#checkout').click()

        //7. Llenar sólo la información de nombre y apellido y dar click en continue.
        cy.get('#first-name').type('Benjamin')
        cy.get('#last-name').type('Garcia')
        cy.get('#continue').click()

        //8. Hacer aserción en el mensaje Error: Postal Code is required
        cy.get('div.page_wrapper div.checkout_info_container div.checkout_info_wrapper form:nth-child(1) div.checkout_info > div.error-message-container.error:nth-child(4)').should('have.text', 'Error: Postal Code is required')

        //9. LLenar el código postal y dar click en continue.
        cy.get('#postal-code').type('03630')
        cy.get('#continue').click()

        //10. Click en finish
        cy.get('#finish').click()

        //11.Hacer aserción en el mensaje THANK YOU FOR YOUR ORDER
        cy.get('div:nth-child(2) div.page_wrapper div:nth-child(1) div.checkout_complete_container > h2.complete-header').should('have.text', 'THANK YOU FOR YOUR ORDER')
    })
    
})
