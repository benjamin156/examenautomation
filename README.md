## Nombre

QA Automation prueba técnica 

## Descripción

Se realiza la utomatizacion de una compra en linea en la pagina de https://www.saucedemo.com/

Referecnia de implementación: https://drive.google.com/file/d/1OATDThWxvZidZJQS_nDYi33dQkuseE_F/view

## Instalación

Para poder ejecutar este proyecto es necesario lo siguiente:

1.- Tener instalada la librería de NodeJs en tu equipo, instalar por medio de brew con el siguiente comando  brew install node 
2.- Asegurar que se haya instalado correctamente NodeJS   node -v ó npm -v
3.- Clonar el poruyecto usando git: git clone git@gitlab.com:benjamin156/examenautomation.git
4.- Abrir el proyecto con visual studio code o cualquier otro editor de texto
5.- Iniciar la terminal
6.- Ejecutar el comando  npm init 
7.- Configurar los valores deseados
8.- Instalar Cypress npm install --save-dev cypress
9.- Integrar los siguientes scripts en scripts del archivo package.json: 
    "runHeaded": "./node_modules/.bin/cypress run --headed --browser chrome" para correcto de manera visual
    "runHeadless": "./node_modules/.bin/cypress run --headless --browser chrome"  para correrlo en modo consola
10.- Ejecutar en la terminal  npm run runHeaded para ver la ejecucion visualmente o npm run runHeadless para ver la ejecucion en modo consola.

## Instalación y configuración para generar el reporte y video de ejecución

1.- Abrir la terminal en Visual studio code o el cualquier editor
2.- Ejecutar el comando: npm i --save-dev cypress-mochawesome-reporter
3.- Validar que en el archivo package.json se haya generado la dependencia "cypress-mochawesome-reporter"
4.- Agregar en el archivo cypress.json lo siguiente:

    "screenshotOnRunFailure": true,  // sacar screenshot
    "reporter": "cypress-mochawesome-reporter",  // Nombre del plugin o dependencia
    "reporterOptions": {
      "reportDir": "cypress/report",
      "charts": true,           // Habilitar graficos
      "reportPageTitle": "Reporte de la ejecución",  // Especificar nombre del reporte
      "embeddedScreenshots": true   // sacar screenshot cuando un tc falle
    }

    Se agregar el nombre del plugin del reporte y variable para especificar su comportamiento

5.-  Ir al folder de support y abrir el archivo index.js
6.- Agregar el siguiente comando para importar la libreria: import 'cypress-mochawesome-reporter/register';
7.- Ir al folder de plugins y abrir el archivo index.js
8.- Agregar las siguientes lineas para espeficar la libreria y el pluggin: 
    module.exports = (on, config) => {
    require('cypress-mochawesome-reporter/plugin')(on);
   };

9.- Guardar cambios y jecutar cypress de manera visual o en consola

10.- Una vez finalizada la ejecucion se creara la carpeta report y dentro tendra un archivo llamado index.html el cual es el reporte, para verlo tenemos que ingresar al explorador de archivos de nuestro equipo ubicar la carpeta y dar doble click en index.html

Adicional tambien se crea una carpeta llamada videos, dentro de ella se genera una carpeta con el nombre de nuestro proyecto y dentreo de esa carpeta se genera un video que contine toda la ejeccion de los tcs.

De la misma manera tenemos que ingresar al explorador de archivos de nuestro equipo ubicar la carpeta y dar doble click en el video.

## Soporte
Cypress: https://docs.cypress.io/guides/overview/why-cypress
Plugin reporte: https://github.com/LironEr/cypress-mochawesome-reporter/tree/v2

## Autor
Benjamin Garcia Garcia
